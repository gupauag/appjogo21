package br.com.itau;

public class Cartas {

    protected int pesoCartaBaralho;
    protected int pesoNipe;

    protected String carta;
    protected String nipe;

//    public Cartas(CartasBaralho cartasBaralho, Nipes nipes) {
//        this.cartasBaralho = cartasBaralho;
//        this.nipes = nipes;
//    }

    public int getPesoCartaBaralho() {
        return pesoCartaBaralho;
    }

    public void setPesoCartaBaralho(int pesoCartaBaralho) {
        this.pesoCartaBaralho = pesoCartaBaralho;
    }

    public int getPesoNipe() {
        return pesoNipe;
    }

    public void setPesoNipe(int pesoNipe) {
        this.pesoNipe = pesoNipe;
    }

    public String getCarta() {
        return carta;
    }

    public void setCarta(String carta) {
        this.carta = carta;
    }

    public String getNipe() {
        return nipe;
    }

    public void setNipe(String nipe) {
        this.nipe = nipe;
    }
}
