package br.com.itau;

public enum Nipes {
    COPAS(1),
    OURO(2),
    PAUS(3),
    ESPADA(4);

    int valor;

    Nipes(int valor) {
        this.valor = valor;
    }

    public int getValorNipe() {
        return valor;
    }
}