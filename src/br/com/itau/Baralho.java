package br.com.itau;


import java.util.*;

public class Baralho extends Cartas{

    private static List<Cartas> deck;

    public Baralho() {
        deck = new ArrayList<Cartas>();
    }

    public static List<Cartas> getDeck() {
        return deck;
    }

    public static void setDeck(List<Cartas> deck) {
        Baralho.deck = deck;
    }

    public void criaBaralho(){
        Cartas novaCarta;

        for(Nipes nipe: Nipes.values()){
            for (CartasBaralho carta: CartasBaralho.values()) {
                novaCarta = new Cartas();

                novaCarta.setPesoNipe(nipe.valor);
                novaCarta.setPesoCartaBaralho(carta.valor);

                novaCarta.setNipe(nipe.name());
                novaCarta.setCarta(carta.name());

                deck.add(novaCarta);
                //System.out.println("Nipe: "+nipe.valor+"|Carta: "+carta.valor + " = "+carta.name()+" de "+nipe.name());
            }
        }

        embaralhaBaralho();

        //System.out.println("VALORRR "+deck.get(0).getCarta());
//        for(Cartas cart: deck){
//            System.out.println("Carta: "+cart.getNipe() +" - "+cart.getPesoCartaBaralho());
//        }

    }

    public void embaralhaBaralho(){
        //Embaralha o baralho!
        Collections.shuffle(getDeck());
    }



    public Cartas sorteiraCarta(){
        Cartas carta = new Cartas();
        carta = getDeck().get(0);
        deck.remove(carta);
        return carta;
    }

}
