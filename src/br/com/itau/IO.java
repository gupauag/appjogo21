package br.com.itau;

import sun.awt.X11.XSystemTrayPeer;

import java.io.IOException;
import java.util.Scanner;

public class IO {
    Baralho baralho = new Baralho();
    Scanner scanner = new Scanner(System.in);
    Cartas carta = new Cartas();

    private String maoJogador = "";
    private String maoDealer = "";

    String nomeJogador;
    int melhorPontuacao = 0;

    public void comecaJogo() throws InterruptedException, IOException {

        boolean sair = false;

        int somaDealer;
        int somaJogador;
        boolean saiDaJogada;
        int jogadas;
        boolean parou;

        System.out.println("------JOGO 21-------");
        System.out.print("Digite seu nome: ");
        nomeJogador = scanner.nextLine();

        while (!sair) {
            System.out.println("------NOVA PARTIDA-------");

            baralho.criaBaralho();

            somaDealer = 0;
            somaJogador = 0;
            jogadas = 1;
            saiDaJogada = false;
            parou = false;
            maoJogador = "";
            maoDealer = "";

            while (!saiDaJogada) {

                carta = baralho.sorteiraCarta();
                if (jogadas == 1) {
                    somaDealer = somaDealer + carta.getPesoCartaBaralho();
                    System.out.println("Dealer com: " + getMaoDealer(carta.getNipe(), carta.getCarta())+" Soma = "+somaDealer);
                    jogadas++;
                } else if (jogadas <= 3) {
                    //Thread.sleep(1000);
                    somaJogador = somaJogador + carta.getPesoCartaBaralho();
                    System.out.println("Jogador com: " + getMaoJogador(carta.getNipe(), carta.getCarta())+" Soma = "+somaJogador);
                    jogadas++;
                }else{
                    if(!parou){
                        System.out.print("Mais uma carta S/N :");
                        if (scanner.nextLine().toUpperCase().equals("S")) {
                            somaJogador = somaJogador + carta.getPesoCartaBaralho();
                            System.out.println("Jogador com: " + getMaoJogador(carta.getNipe(), carta.getCarta())+" Soma = "+somaJogador);
                            jogadas++;

                            if (somaJogador > 21) {
                                System.out.println("Você PERDEU. A soma das cartas deu mais q 21.");
                                saiDaJogada = true;
                            } else if (somaJogador == 21) {
                                System.out.println("Você Ganhou. BlackJack!!!!");
                                melhorPontuacao = 21;
                                saiDaJogada = true;
                            }
                        }else parou = true;
                    }else{
                        parou = true;
                        if (somaDealer <= somaJogador) {

                            //Thread.sleep(2000);
                            somaDealer = somaDealer + carta.getPesoCartaBaralho();
                            System.out.println("Dealer com: " + getMaoDealer(carta.getNipe(), carta.getCarta())+" Soma = "+somaDealer);
                            jogadas++;

                            if (somaDealer > 21) {
                                Thread.sleep(2000);
                                System.out.println("Você Ganhou. Dealer estourou 21.");
                                if (somaJogador > melhorPontuacao) {
                                    melhorPontuacao = somaJogador;
                                }
                                saiDaJogada = true;
                            } else if (somaDealer == 21) {
                                Thread.sleep(2000);
                                System.out.println("Você PERDEU. Dealer com BlackJack!!!!");
                                saiDaJogada = true;
                            } else if (somaDealer > somaJogador) {
                                Thread.sleep(2000);
                                System.out.println("Você PERDEU. Dealer com maior mão!");
                                saiDaJogada = true;
                            }
                        }
                    }
                }

            }


            System.out.print("Deseja jogar novamente S/N: ");
            if (scanner.nextLine().toUpperCase().equals("N")) {
                sair = true;
            }else{
                System.out.println("\n");
            }
        }

        System.out.println("\n------FIM DE JOGO-------");
        System.out.println("Obrigado, " + nomeJogador + " por jogar 21.");
        System.out.println("Sua melhor pontuação foi: " + melhorPontuacao + " pontos");


    }

    public String getMaoDealer(String nipe, String carta) {
        if (maoDealer.equals("")) maoDealer = "(" + carta + " de " + nipe + ")";
        else maoDealer = maoDealer + " - (" + carta + " de " + nipe + ")";
        return maoDealer;
    }

    public String getMaoJogador(String nipe, String carta) {
        if (maoJogador.equals("")) maoJogador = "(" + carta + " de " + nipe + ")";
        else maoJogador = maoJogador + " - (" + carta + " de " + nipe + ")";
        return maoJogador;
    }

}
